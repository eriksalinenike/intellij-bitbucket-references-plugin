package com.atlassian.bitbucket.idea.configuration;

import com.intellij.util.Base64;
import org.apache.commons.httpclient.Header;

public class DefaultBitbucketAuthentication implements BitbucketAuthentication {
    private final String username;
    private final String password;

    public DefaultBitbucketAuthentication(String username, String password) {
        this.username = username;
        this.password = password;
    }

    @Override
    public Header getAuthHeader() {
        return new Header("Authorization", "Basic " + Base64.encode((username + ":" + password).getBytes()));
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public String getPassword() {
        return password;
    }
}
