package com.atlassian.bitbucket.idea.configuration;

import com.intellij.ide.passwordSafe.PasswordSafe;
import com.intellij.ide.passwordSafe.PasswordSafeException;
import com.intellij.openapi.components.*;
import com.intellij.openapi.diagnostic.Logger;
import com.intellij.util.xmlb.XmlSerializerUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/*
 * This file was derived from code in https://github.com/ktisha/Crucible4IDEA/
 *
 * Copyright (c) 2013-2015 Ekaterina Tuzova (ktisha)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
 * EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 * AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS IN THE SOFTWARE.
 */
@State(name = "BitbucketSettings",
        storages = {
                @Storage(file = StoragePathMacros.APP_CONFIG + "/bitbucket.xml")
        }
)
public class DefaultBitbucketSettingsService implements BitbucketSettingsService, PersistentStateComponent<DefaultBitbucketSettingsService> {

    private static final Logger LOG = Logger.getInstance(BitbucketConfigurable.class.getName());
    public static final String PASSWORD_KEY = "BITBUCKET_AUTHENTICATION_SETTINGS";

    public String username = "";

    @NotNull
    @Override
    public BitbucketAuthentication getAuthentication() {
        return new DefaultBitbucketAuthentication(username, getPassword());
    }

    @Override
    public void saveAuthentication(BitbucketAuthentication authentication) {
        username = authentication.getUsername();
        try {
            PasswordSafe.getInstance().storePassword(null, getClass(), PASSWORD_KEY, authentication.getPassword());
        } catch (PasswordSafeException e) {
            LOG.warn("Couldn't save password.  Key: " + PASSWORD_KEY, e);
        }
    }

    @Override
    public DefaultBitbucketSettingsService getState() {
        return this;
    }

    @Override
    public void loadState(DefaultBitbucketSettingsService state) {
        XmlSerializerUtil.copyBean(state, this);
    }

    public static BitbucketSettingsService getInstance() {
        return ServiceManager.getService(BitbucketSettingsService.class);
    }

    @Nullable
    private String getPassword() {
        try {
            return PasswordSafe.getInstance().getPassword(null, this.getClass(), PASSWORD_KEY);
        }
        catch (PasswordSafeException e) {
            LOG.info("Couldn't get the Bitbucket password", e);
            return "";
        }
    }
}
