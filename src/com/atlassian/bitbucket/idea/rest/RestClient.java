package com.atlassian.bitbucket.idea.rest;

import com.google.gson.JsonObject;

import java.io.IOException;

public interface RestClient {

    JsonObject get(String path) throws IOException;

    int post(String path, String jsonBody) throws IOException;

    int delete(String path) throws IOException;
}
