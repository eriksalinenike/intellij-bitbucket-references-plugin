package com.atlassian.bitbucket.idea.pullrequest.rest;

/*
 * Represents one changed file of the Pull Request:
 *
 * {
 *     "contentId": "15f4445ec5ef688d24c1344dec86bb7b6775b940",
 *     "executable": false,
 *     "fromContentId": "4f79c8d0cadc382b81729369bfe1fc8b2439fa07",
 *     "links": {
 *         "self": [
 *             null
 *         ]
 *     },
 *     "nodeType": "FILE",
 *     "path": {
 *         "components": [
 *             "plugins",
 *             "git",
 *             "src",
 *             "test",
 *             "java",
 *             "com",
 *             "atlassian",
 *             "stash",
 *             "internal",
 *             "scm",
 *             "git",
 *             "DefaultGitCompareCommandFactoryTest.java"
 *         ],
 *         "extension": "java",
 *         "name": "DefaultGitCompareCommandFactoryTest.java",
 *         "parent": "plugins/git/src/test/java/com/atlassian/stash/internal/scm/git",
 *         "toString": "plugins/git/src/test/java/com/atlassian/stash/internal/scm/git/DefaultGitCompareCommandFactoryTest.java"
 *     },
 *     "percentUnchanged": -1,
 *     "properties": {
 *         "activeComments": 3,
 *         "orphanedComments": 1
 *     },
 *     "srcExecutable": false,
 *     "type": "MODIFY"
 * }
 */
public class RestChangedFile {

    private String type;
    private Path path;
    private Properties properties;

    public String getChangeType() {
        return type;
    }

    public String getName() {
        return path == null ? null : path.getName();
    }

    public String getRelativePath() {
        return path == null ? null : path.getRelativePath();
    }

    public int getActiveCommentsNumber() {
        return properties == null ? 0 : properties.activeComments;
    }

    public int getOrphanedCommentsNUmber() {
        return properties == null ? 0 : properties.orphanedComments;
    }

    private static class Path {
        private String name;
        private String toString;

        public String getName() {
            return name;
        }

        public String getRelativePath() {
            return toString;
        }
    }

    private static class Properties {
        private int activeComments;
        private int orphanedComments;
    }
}
