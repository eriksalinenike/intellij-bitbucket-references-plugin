package com.atlassian.bitbucket.idea.pullrequest.comment.ui;

import com.atlassian.bitbucket.idea.pullrequest.PullRequestService;
import com.intellij.openapi.components.AbstractProjectComponent;
import com.intellij.openapi.components.ServiceManager;
import com.intellij.openapi.fileEditor.FileEditorManagerListener;
import com.intellij.openapi.project.Project;

public class CommentComponent extends AbstractProjectComponent {
    private final FileEditorCommentsInjector injector;

    protected CommentComponent(Project project) {
        super(project);
        PullRequestService pullRequestService = ServiceManager.getService(project, PullRequestService.class);
        injector = new FileEditorCommentsInjector(pullRequestService);
    }

    @Override
    public void initComponent() {
        myProject.getMessageBus().connect().subscribe(FileEditorManagerListener.FILE_EDITOR_MANAGER, injector);
    }
}
