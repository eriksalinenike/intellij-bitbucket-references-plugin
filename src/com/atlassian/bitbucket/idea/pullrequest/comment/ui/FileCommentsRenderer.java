package com.atlassian.bitbucket.idea.pullrequest.comment.ui;

import com.atlassian.bitbucket.idea.pullrequest.comment.model.FileCommentsModel;
import com.atlassian.bitbucket.idea.pullrequest.comment.model.LineCommentsBlock;
import com.intellij.openapi.Disposable;
import com.intellij.openapi.editor.Editor;

import java.util.HashMap;
import java.util.Map;

public class FileCommentsRenderer implements Disposable, CommentsListener {

    private final Editor editor;
    private final FileCommentsModel fileCommentsModel;

    private final Map<LineCommentsBlock, LineCommentsBlockRenderer> commentsBlockRenderers;

    public FileCommentsRenderer(Editor editor, FileCommentsModel fileCommentsModel) {
        this.editor = editor;
        this.fileCommentsModel = fileCommentsModel;
        this.commentsBlockRenderers = new HashMap<LineCommentsBlock, LineCommentsBlockRenderer>();

        editor.getProject().getMessageBus().connect().subscribe(CommentsListener.COMMENTS_UPDATED_TOPIC, this);

        commentsUpdated();
    }

    public void hidePopups() {
        for (LineCommentsBlockRenderer commentsBlockRenderer : commentsBlockRenderers.values()) {
            commentsBlockRenderer.hideBalloon();
        }
    }

    @Override
    public void dispose() {
        for (LineCommentsBlockRenderer commentsBlockRenderer : commentsBlockRenderers.values()) {
            commentsBlockRenderer.dispose();
        }
        commentsBlockRenderers.clear();
    }

    @Override
    public void commentsUpdated() {
        for (LineCommentsBlock lineCommentsBlock : fileCommentsModel.getLineCommentsBlocks()) {
            LineCommentsBlockRenderer renderer = commentsBlockRenderers.get(lineCommentsBlock);
            if (renderer == null) {
                renderer = new LineCommentsBlockRenderer(editor, lineCommentsBlock);
                commentsBlockRenderers.put(lineCommentsBlock, renderer);
            }
        }
    }
}
