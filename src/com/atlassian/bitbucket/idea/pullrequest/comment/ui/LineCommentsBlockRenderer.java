package com.atlassian.bitbucket.idea.pullrequest.comment.ui;

import com.atlassian.bitbucket.linky.Constants;
import com.atlassian.bitbucket.idea.pullrequest.comment.model.LineCommentsBlock;
import com.intellij.openapi.Disposable;
import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.editor.LogicalPosition;
import com.intellij.openapi.editor.markup.GutterIconRenderer;
import com.intellij.openapi.editor.markup.HighlighterLayer;
import com.intellij.openapi.editor.markup.RangeHighlighter;
import com.intellij.openapi.editor.markup.TextAttributes;
import com.intellij.openapi.ui.popup.Balloon;
import com.intellij.openapi.ui.popup.BalloonBuilder;
import com.intellij.openapi.ui.popup.JBPopupFactory;
import com.intellij.ui.JBColor;
import com.intellij.ui.awt.RelativePoint;
import com.intellij.util.ui.PositionTracker;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import java.awt.*;


public class LineCommentsBlockRenderer implements Disposable, CommentsListener {
    private static final JBColor HIGHLIGHT_COLOR = JBColor.ORANGE;
    private static final int HIGHLIGHT_LEVEL = HighlighterLayer.CARET_ROW + 1;

    private final Editor editor;
    private final LineCommentsBlock lineCommentsBlock;

    private RangeHighlighter rangeHighlighter;
    private final ToggleBalloonRunnable toggleBalloonRunnable;

    public LineCommentsBlockRenderer(Editor editor, LineCommentsBlock lineCommentsBlock) {
        this.editor = editor;
        this.lineCommentsBlock = lineCommentsBlock;

        editor.getProject().getMessageBus().connect().subscribe(CommentsListener.COMMENTS_UPDATED_TOPIC, this);

        toggleBalloonRunnable = new ToggleBalloonRunnable(lineCommentsBlock, editor);
        ToggleCommentsBlockBalloonAction balloonAction = new ToggleCommentsBlockBalloonAction(toggleBalloonRunnable);

        createLineHighlighter(balloonAction);
    }

    public void hideBalloon() {
        if (toggleBalloonRunnable != null) {
            toggleBalloonRunnable.hideBalloon();
        }
    }

    public void updateBalloon() {
        if (toggleBalloonRunnable != null) {
            toggleBalloonRunnable.updateBalloon();
        }
    }

    @Override
    public void dispose() {
        hideBalloon();
        if (rangeHighlighter != null) {
            editor.getMarkupModel().removeHighlighter(rangeHighlighter);
            rangeHighlighter.dispose();
        }
    }

    private void createLineHighlighter(AnAction clickAction) {
        TextAttributes textAttributes = new TextAttributes();
        textAttributes.setBackgroundColor(HIGHLIGHT_COLOR);

        int lineNumber = lineCommentsBlock.getLineNumber() - 1; // IDEA counts lines from 0

        rangeHighlighter = editor.getMarkupModel()
                .addLineHighlighter(lineNumber, HIGHLIGHT_LEVEL, textAttributes);

        rangeHighlighter.setErrorStripeMarkColor(HIGHLIGHT_COLOR);

//        TODO move in update() method
//        if (stripeMarkTooltip != null) {
//            rangeHighlighter.setErrorStripeTooltip(stripeMarkTooltip);
//        }

        rangeHighlighter.setGutterIconRenderer(new CommentGutterIconRenderer(clickAction, lineNumber));
    }

    @Override
    public void commentsUpdated() {
        updateBalloon();
    }

    private static class ToggleBalloonRunnable implements Runnable, BalloonListener {

        private final LineCommentsBlock lineCommentsBlock;
        private final Editor editor;

        private Balloon balloon;
        private PositionTracker<Balloon> positionTracker;
        private JPanel balloonPanel;

        private ToggleBalloonRunnable(LineCommentsBlock lineCommentsBlock, Editor editor) {
            this.lineCommentsBlock = lineCommentsBlock;
            this.editor = editor;
            editor.getProject().getMessageBus().connect().subscribe(BalloonListener.BALLOON_TOPIC, this);
        }

        @Override
        public void run() {
            synchronized (lineCommentsBlock) {
                if (balloon == null) {
                    showBalloon();
                } else {
                    hideBalloon();
                }
            }
        }

        public void hideBalloon() {
            synchronized (lineCommentsBlock) {
                if (balloon != null) {
                    balloon.hide();
                    balloon.dispose();
                    balloon = null;
                    balloonPanel = null;

                    positionTracker.dispose();
                    positionTracker = null;
                }
            }
        }

        public void showBalloon() {
            synchronized (lineCommentsBlock) {
                createBalloon();
                createBalloonPositionTracker();

                BalloonListener balloonListener = editor.getProject().getMessageBus().syncPublisher(BalloonListener.BALLOON_TOPIC);
                balloonListener.showBalloonRequested(balloon);

                balloon.show(positionTracker, Balloon.Position.below);
            }
        }

        public void updateBalloon() {
            synchronized (lineCommentsBlock) {
                if (balloon != null) {
                    balloonPanel.removeAll();
                    balloonPanel.add(CommentsUIHelper.buildCommentsBlockPanel(lineCommentsBlock));
                    balloonPanel.updateUI();
                    balloon.revalidate();
                }
            }
        }

        private void createBalloon() {
            balloonPanel = new JPanel();
            balloonPanel.add(CommentsUIHelper.buildCommentsBlockPanel(lineCommentsBlock));
            BalloonBuilder builder = JBPopupFactory.getInstance()
                    .createDialogBalloonBuilder(balloonPanel, "Comments");
            builder.setCloseButtonEnabled(false);
            balloon = builder.setShadow(true).createBalloon();
        }

        private void createBalloonPositionTracker() {
            int lineNumber = lineCommentsBlock.getLineNumber();
            if (balloon == null) {
                throw new IllegalStateException("Comments block balloon for line " + lineNumber + " should be created first");
            }
            final LogicalPosition position = new LogicalPosition(lineNumber, 0);
            final Dimension preferredSize = balloon.getPreferredSize();

            positionTracker = new PositionTracker<Balloon>(editor.getContentComponent()) {
                @Override
                public RelativePoint recalculateLocation(Balloon balloon) {
                    Point point = editor.logicalPositionToXY(position);
                    point.translate(preferredSize.width / 2 + 5, 0);
                    return new RelativePoint(editor.getContentComponent(), point);
                }
            };
        }

        @Override
        public void showBalloonRequested(Balloon balloon) {
            if (!balloon.equals(this.balloon)) {
                hideBalloon();
            }
        }
    }


    private static class CommentGutterIconRenderer extends GutterIconRenderer {
        private final AnAction gutterIconClickAction;
        private final int lineNumber;

        private CommentGutterIconRenderer(AnAction gutterIconClickAction, int lineNumber) {
            this.gutterIconClickAction = gutterIconClickAction;
            this.lineNumber = lineNumber;
        }

        @NotNull
        @Override
        public Icon getIcon() {
            return Constants.BITBUCKET_ICON;
        }

        @Nullable
        @Override
        public AnAction getClickAction() {
            return gutterIconClickAction;
        }

        @Override
        public boolean equals(Object obj) {
            return obj instanceof CommentGutterIconRenderer &&
                    lineNumber == ((CommentGutterIconRenderer) obj).lineNumber;
        }

        @Override
        public int hashCode() {
            return lineNumber;
        }
    }
}
