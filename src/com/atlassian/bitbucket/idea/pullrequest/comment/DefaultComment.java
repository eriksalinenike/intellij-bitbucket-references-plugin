package com.atlassian.bitbucket.idea.pullrequest.comment;

import com.atlassian.bitbucket.idea.pullrequest.User;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collections;
import java.util.Date;
import java.util.List;

public class DefaultComment implements Comment {
    private final CommentAnchor anchor;
    private final long id;
    private final String text;
    private final String htmlText;
    private final User commentator;
    private final Date createdDate;
    private final Date updatedDate;
    private final List<Comment> replies;
    private final boolean isEditable;
    private final boolean isDeletable;
    private final int version;

    private DefaultComment(Builder builder) {
        anchor = builder.anchor;
        id = builder.id;
        text = builder.text;
        htmlText = builder.htmlText;
        commentator = builder.commentator;
        createdDate = builder.createdDate;
        updatedDate = builder.updatedDate;
        replies = builder.replies;
        isEditable = builder.isEditable;
        isDeletable = builder.isDeletable;
        version = builder.version;
    }

    public static Builder builder() {
        return new Builder();
    }

    @Nullable
    @Override
    public CommentAnchor getAnchor() {
        return anchor;
    }

    @Override
    public long getId() {
        return id;
    }

    @NotNull
    @Override
    public String getText() {
        return text;
    }

    @Nullable
    @Override
    public String getHtmlText() {
        return htmlText;
    }

    @NotNull
    @Override
    public User getCommentator() {
        return commentator;
    }

    @NotNull
    @Override
    public Date getCreatedDate() {
        return createdDate;
    }

    @NotNull
    @Override
    public Date getUpdatedDate() {
        return updatedDate;
    }

    @NotNull
    @Override
    public List<Comment> getReplyComments() {
        return replies;
    }

    @Override
    public boolean isEditable() {
        return isEditable;
    }

    @Override
    public boolean isDeletable() {
        return isDeletable;
    }

    @Override
    public int getVersion() {
        return version;
    }

    public static final class Builder {
        private CommentAnchor anchor;
        private long id;
        private String text;
        private String htmlText;
        private User commentator;
        private Date createdDate;
        private Date updatedDate;
        private List<Comment> replies;
        private boolean isEditable;
        private boolean isDeletable;
        private int version;

        private Builder() {
            replies = Collections.emptyList();
        }

        public Builder anchor(@NotNull CommentAnchor anchor) {
            this.anchor = anchor;
            return this;
        }

        public Builder id(long id) {
            this.id = id;
            return this;
        }

        public Builder text(@NotNull String text) {
            this.text = text;
            return this;
        }

        public Builder htmlText(@Nullable String htmlText) {
            this.htmlText = htmlText;
            return this;
        }

        public Builder commentator(@NotNull User commentator) {
            this.commentator = commentator;
            return this;
        }

        public Builder createdDate(@NotNull Date createdDate) {
            this.createdDate = createdDate;
            return this;
        }

        public Builder updatedDate(@NotNull Date updatedDate) {
            this.updatedDate = updatedDate;
            return this;
        }

        public Builder replies(@NotNull List<Comment> replies) {
            this.replies = replies;
            return this;
        }

        public Builder isEditable(boolean isEditable) {
            this.isEditable = isEditable;
            return this;
        }

        public Builder isDeletable(boolean isDeletable) {
            this.isDeletable = isDeletable;
            return this;
        }

        public Builder version(int version) {
            this.version = version;
            return this;
        }

        public DefaultComment build() {
            return new DefaultComment(this);
        }
    }
}
