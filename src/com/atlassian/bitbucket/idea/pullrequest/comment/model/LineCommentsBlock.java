package com.atlassian.bitbucket.idea.pullrequest.comment.model;

import com.atlassian.bitbucket.idea.pullrequest.comment.Comment;
import com.atlassian.bitbucket.idea.pullrequest.comment.CommentsProvider;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Represents comments linked to one line in code
 */
public class LineCommentsBlock {

    private final CommentsProvider lineCommentsProvider;
    /**
     * Line of code that this block relates to
     */
    private final int lineNumber;

    public LineCommentsBlock(int lineNumber, @NotNull CommentsProvider lineCommentsProvider) {
        this.lineNumber = lineNumber;
        this.lineCommentsProvider = lineCommentsProvider;
    }

    public int getLineNumber() {
        return lineNumber;
    }

    @NotNull
    public List<Comment> getComments() {
        List<Comment> comments = new ArrayList<Comment>(lineCommentsProvider.getComments(lineNumber));
        Collections.sort(comments, new Comparator<Comment>() {
            @Override
            public int compare(Comment o1, Comment o2) {
                return o1 == null ?
                        (o2 == null ? 0 : -1) :
                        (o2 == null ? 1 : o1.getCreatedDate().compareTo(o2.getCreatedDate()));
            }
        });
        return comments;
    }

    public void replyToComment(Comment comment, String text) {
        lineCommentsProvider.replyToComment(comment, text);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        LineCommentsBlock that = (LineCommentsBlock) o;
        return lineNumber == that.lineNumber;
    }

    @Override
    public int hashCode() {
        return lineNumber;
    }
}
