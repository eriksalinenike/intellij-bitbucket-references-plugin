package com.atlassian.bitbucket.idea.pullrequest;

import java.util.Date;

public class DefaultPullRequest implements PullRequest {

    private final User author;
    private final long id;
    private final Date created;
    private final String title;

    private DefaultPullRequest(long id, User author, Date created, String title) {
        this.id = id;
        this.author = author;
        this.created = created;
        this.title = title;
    }

    public static DefaultPullRequest build(long id, User author, Date created, String title) {
        return new DefaultPullRequest(id, author, created, title);
    }

    @Override
    public User getAuthor() {
        return author;
    }

    @Override
    public Date getCreatedDate() {
        return created;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public long getId() {
        return id;
    }
}
