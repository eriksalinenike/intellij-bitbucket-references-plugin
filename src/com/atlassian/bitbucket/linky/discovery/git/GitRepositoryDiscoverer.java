package com.atlassian.bitbucket.linky.discovery.git;

import com.atlassian.bitbucket.linky.discovery.VcsRemoteAnalyzer;
import com.atlassian.bitbucket.linky.discovery.VcsRepositoryDiscoverer;
import com.atlassian.bitbucket.linky.repository.BitbucketRepository;
import com.atlassian.bitbucket.linky.util.Optionals;
import com.intellij.openapi.vfs.VirtualFile;
import git4idea.GitUtil;
import git4idea.repo.GitRemote;
import git4idea.repo.GitRepository;
import git4idea.repo.GitRepositoryManager;
import org.jetbrains.annotations.NotNull;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import static java.util.Objects.requireNonNull;
import static java.util.Optional.of;
import static java.util.Optional.ofNullable;
import static java.util.function.Function.identity;
import static java.util.stream.Collectors.toList;

public class GitRepositoryDiscoverer implements VcsRepositoryDiscoverer {

    private final GitRepositoryManager gitRepoManager;
    private final VcsRemoteAnalyzer vcsRemoteAnalyzer;

    public GitRepositoryDiscoverer(@NotNull GitRepositoryManager gitRepoManager,
                                   @NotNull VcsRemoteAnalyzer vcsRemoteAnalyzer) {
        this.gitRepoManager = requireNonNull(gitRepoManager, "gitRepoManager");
        this.vcsRemoteAnalyzer = requireNonNull(vcsRemoteAnalyzer, "vcsRemoteAnalyzer");
    }


    @NotNull
    @Override
    public Map<VirtualFile, BitbucketRepository> discoverBitbucketRepositories() {
        return gitRepoManager.getRepositories().stream()
                .map(repo -> remotesOfRepo(repo).stream()
                        .flatMap(gitRemote -> gitRemote.getUrls().stream())
                        .map(remote -> vcsRemoteAnalyzer.bitbucketRepositoryFor(repo, remote))
                        .flatMap(Optionals::toStream)
                        .findFirst())
                .flatMap(Optionals::toStream)
                .collect(Collectors.toMap(BitbucketRepository::getRootDirectory, identity()));
    }

    @NotNull
    private List<GitRemote> remotesOfRepo(@NotNull GitRepository gitRepo) {
        Collection<GitRemote> remotes = gitRepo.getRemotes();
        Optional<GitRemote> mainRemote = getMainRemote(gitRepo);

        return remotes.stream()
                .sorted((remote1, remote2) ->
                        // move main remote to the beginning, do not reorder other remotes
                        mainRemote
                                .map(main -> main.equals(remote1) ? -1 : (main.equals(remote2) ? 1 : 0))
                                .orElse(0))
                .collect(toList());
    }

    @NotNull
    private Optional<GitRemote> getMainRemote(GitRepository gitRepo) {
        Collection<GitRemote> remotes = gitRepo.getRemotes();

        return ofNullable(GitUtil.getTrackInfoForCurrentBranch(gitRepo))
                .map(trackInfo -> of(trackInfo.getRemote()))
                .orElseGet(() ->
                        ofNullable(GitUtil.getDefaultRemote(remotes))
                                .map(Optional::of)
                                .orElseGet(() ->
                                        remotes.stream()
                                                .findFirst()));
    }

}
