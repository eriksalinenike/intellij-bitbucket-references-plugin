package com.atlassian.bitbucket.linky.service;

import com.intellij.openapi.diagnostic.Logger;

import static com.atlassian.bitbucket.linky.Constants.LOGGER_NAME;

public class DefaultVcsAvailabilityService implements VcsAvailabilityService {

    private static final Logger log = Logger.getInstance(LOGGER_NAME);

    private final boolean isGitAvailable;
    private final boolean isHgAvailable;

    public DefaultVcsAvailabilityService() {
        this.isGitAvailable = isClassAvailable("git4idea.commands.Git");
        this.isHgAvailable = isClassAvailable("org.zmlx.hg4idea.HgVcs");

        if (!isGitAvailable) {
            log.debug("Git seems to be unavailable");
        }
        if (!isHgAvailable) {
            log.debug("Hg seems to be unavailable");
        }
    }

    @Override
    public boolean isGitAvailable() {
        return isGitAvailable;
    }

    @Override
    public boolean isHgAvailable() {
        return isHgAvailable;
    }

    private boolean isClassAvailable(String className) {
        try {
            Class.forName(className);
            return true;
        } catch (ClassNotFoundException e) {
            return false;
        }
    }
}
