package com.atlassian.bitbucket.linky.service.hosting;

import com.atlassian.bitbucket.linky.hosting.DefaultHosting;
import com.atlassian.bitbucket.linky.hosting.Hosting;
import com.intellij.util.xmlb.annotations.Tag;
import org.apache.commons.lang.StringUtils;
import org.jetbrains.annotations.NotNull;

import java.net.URI;

public class HostingBean {

    private String baseUri;
    private String baseRestUri;
    private String id;
    private Hosting.Type type;

    public static HostingBean of(String id, Hosting hosting) {
        HostingBean bean = new HostingBean();
        bean.setBaseUri(hosting.getBaseUri().toString());
        bean.setBaseRestUri(hosting.getBaseRestUri()
                .map(URI::toString)
                .orElse(""));
        bean.setId(id);
        bean.setType(hosting.getType());
        return bean;
    }

    @Tag("baseUri")
    public String getBaseUri() {
        return baseUri;
    }

    public void setBaseUri(@NotNull String baseUri) {
        this.baseUri = baseUri;
    }

    @Tag("baseRestUri")
    public String getBaseRestUri() {
        return baseRestUri;
    }

    public void setBaseRestUri(@NotNull String baseRestUri) {
        this.baseRestUri = baseRestUri;
    }

    @Tag("id")
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Tag("type")
    public Hosting.Type getType() {
        return type;
    }

    public void setType(Hosting.Type type) {
        this.type = type;
    }

    public Hosting toHosting() {
        DefaultHosting.Builder builder = type == Hosting.Type.CLOUD ? DefaultHosting.cloud() : DefaultHosting.server();
        if (StringUtils.isNotEmpty(baseRestUri)) {
            builder.baseRestUri(URI.create(baseRestUri));
        }
        return builder.baseUri(URI.create(baseUri)).build();
    }
}
