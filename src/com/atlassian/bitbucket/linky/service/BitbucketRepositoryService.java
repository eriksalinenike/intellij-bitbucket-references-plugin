package com.atlassian.bitbucket.linky.service;

import com.atlassian.bitbucket.linky.BitbucketLinky;
import com.atlassian.bitbucket.linky.repository.BitbucketRepository;
import com.intellij.openapi.vfs.VirtualFile;
import org.jetbrains.annotations.NotNull;

import java.util.Optional;

public interface BitbucketRepositoryService {

    /**
     * Returns {@link BitbucketLinky Bitbucket Linky} for given {@link VirtualFile file}
     */
    Optional<BitbucketLinky> getBitbucketLinky(@NotNull VirtualFile file);

    /**
     * Returns {@link BitbucketRepository Bitbucket repository} that corresponds to project root directory
     */
    Optional<BitbucketRepository> getBitbucketRepository();

    /**
     * Returns {@link BitbucketRepository Bitbucket repository} for given {@link VirtualFile file}
     */
    Optional<BitbucketRepository> getBitbucketRepository(@NotNull VirtualFile file);
}
