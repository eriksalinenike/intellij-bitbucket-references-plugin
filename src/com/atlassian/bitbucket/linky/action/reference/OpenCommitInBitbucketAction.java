package com.atlassian.bitbucket.linky.action.reference;

import com.atlassian.bitbucket.linky.BitbucketLinky;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.fileEditor.FileDocumentManager;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vcs.AbstractVcs;
import com.intellij.openapi.vcs.annotate.FileAnnotation;
import com.intellij.openapi.vcs.annotate.LineNumberListener;
import com.intellij.openapi.vcs.history.VcsRevisionNumber;
import com.intellij.openapi.vcs.impl.UpToDateLineNumberProviderImpl;
import com.intellij.openapi.vfs.VirtualFile;
import org.jetbrains.annotations.NotNull;

import java.net.URI;

import static java.util.Optional.ofNullable;

public class OpenCommitInBitbucketAction extends AbstractBitbucketReferenceAction implements LineNumberListener {

    private FileAnnotation myAnnotation;
    private int lineNumber = -1;

    public OpenCommitInBitbucketAction(FileAnnotation fileAnnotation) {
        super("Open Commit in Bitbucket", "Open selected Commit in Bitbucket");
        this.myAnnotation = fileAnnotation;
    }

    @Override
    public void consume(Integer integer) {
        lineNumber = integer;
    }

    @Override
    protected void actionPerformed(@NotNull AnActionEvent event,
                                   @NotNull Project project,
                                   @NotNull VirtualFile file,
                                   @NotNull BitbucketLinky linky,
                                   @NotNull AbstractVcs vcs) {
        int actualLineNumber = getActualLineNumber(project, file, lineNumber);
        if (actualLineNumber >= 0) {
            VcsRevisionNumber revisionNumber = myAnnotation.getLineRevisionNumber(actualLineNumber);
            if (revisionNumber != null) {
                URI uri = linky.getCommitViewUri(prepareRevisionHash(revisionNumber), file, actualLineNumber + 1);
                openInBrowser(project, uri);
            }
        }
    }

    @Override
    protected boolean couldActionBeEnabled(@NotNull Project project, @NotNull VirtualFile file) {
        return getActualLineNumber(project, file, lineNumber) >= 0;
    }

    private int getActualLineNumber(Project project, VirtualFile file, int lineNumber) {
        return ofNullable(FileDocumentManager.getInstance().getDocument(file))
                .map(document -> new UpToDateLineNumberProviderImpl(document, project).getLineNumber(lineNumber))
                .orElse(-1);
    }

    private String prepareRevisionHash(VcsRevisionNumber vcsRevisionNumber) {
        String revision = vcsRevisionNumber.asString();
        // hg revision hash is prefixed with revision number separated by colon
        if (revision.contains(":")) {
            revision = revision.substring(revision.indexOf(":") + 1);
        }
        return revision;
    }
}
