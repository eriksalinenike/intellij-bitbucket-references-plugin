package com.atlassian.bitbucket.linky.action.reference;

import com.atlassian.bitbucket.linky.BitbucketLinky;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vcs.AbstractVcs;
import com.intellij.openapi.vfs.VirtualFile;
import org.jetbrains.annotations.NotNull;


public class BitbucketCreatePullRequestAction extends AbstractBitbucketReferenceAction {

    public BitbucketCreatePullRequestAction() {
        super("Bitbucket Pull Request", "Create Bitbucket Pull Request");

    }

    @Override
    protected void actionPerformed(@NotNull AnActionEvent event,
                                   @NotNull Project project,
                                   @NotNull VirtualFile file,
                                   @NotNull BitbucketLinky linky,
                                   @NotNull AbstractVcs vcs) {
        openInBrowser(project, linky.getPullRequestUri());
    }

}
