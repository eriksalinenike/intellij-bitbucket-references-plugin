package com.atlassian.bitbucket.linky.util;

import java.util.Optional;
import java.util.function.Consumer;
import java.util.stream.Stream;

import static java.util.Optional.empty;
import static java.util.Optional.of;

/**
 * Helper methods to simplify common operations on {@link Optional Optionals}.
 */
public class Optionals {
    private Optionals() {
        throw new UnsupportedOperationException(getClass().getSimpleName() + " only contains static utility methods " +
                "and should not be instantiated.");
    }

    /**
     * Turn the given {@code Optional<T>} into a {@code Stream<T>} of a single element.
     *
     * @param optional value
     * @param <T>      the type of value
     * @return Stream of the value contained in the {@code Optional}, {@link Stream#empty()} otherwise.
     */
    // This will be in JDK 9: https://bugs.openjdk.java.net/browse/JDK-8050820
    public static <T> Stream<T> toStream(Optional<T> optional) {
        if (optional.isPresent()) {
            return Stream.of(optional.get());
        }
        return Stream.empty();
    }

    public static Optional<Integer> parseInteger(String string, Consumer<NumberFormatException> exceptionConsumer) {
        try {
            return of(Integer.valueOf(string));
        } catch (NumberFormatException e) {
            exceptionConsumer.accept(e);
            return empty();
        }
    }
}