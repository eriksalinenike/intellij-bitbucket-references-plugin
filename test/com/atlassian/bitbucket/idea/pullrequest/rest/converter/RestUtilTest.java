package com.atlassian.bitbucket.idea.pullrequest.rest.converter;

import com.atlassian.bitbucket.idea.pullrequest.rest.*;
import com.google.common.base.Charsets;
import com.google.common.io.Resources;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.json.JSONException;
import org.junit.Test;

import java.io.IOException;
import java.net.URL;
import java.util.Date;
import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.Assert.assertThat;
import static org.skyscreamer.jsonassert.JSONAssert.assertEquals;

public class RestUtilTest {

    @Test
    public void testParseChangedFiles() {
        JsonObject json = readJsonFromResource("changes.json");
        List<RestChangedFile> changes = RestUtil.parseChangedFiles(json);

        assertThat(changes.size(), is(7));

        RestChangedFile restChangedFile = changes.get(1);

        assertThat(restChangedFile.getName(), is("DefaultGitCompareCommandFactoryTest.java"));
        assertThat(restChangedFile.getRelativePath(), is("plugins/git/src/test/java/com/atlassian/stash/internal/scm/git/DefaultGitCompareCommandFactoryTest.java"));
        assertThat(restChangedFile.getChangeType(), is("MODIFY"));
        assertThat(restChangedFile.getActiveCommentsNumber(), is(3));
        assertThat(restChangedFile.getOrphanedCommentsNUmber(), is(1));
    }

    @Test
    public void testParseComments() {
        JsonObject json = readJsonFromResource("comments.json");
        List<RestComment> restComments = RestUtil.parseComments(json);

        assertThat(restComments.size(), is(2));

        RestComment first = restComments.get(0);
        assertThat(first.getId(), is(131703L));
        assertThat(first.getVersion(), is(8));
        assertThat(first.getText(), is("Hey guys, I want to comment here too."));

        RestUser author = first.getAuthor();
        assertThat(author, is(notNullValue()));
        assertThat(author.getName(), is("dpenkin"));
        assertThat(author.getSlug(), is("dpenkin"));
        assertThat(author.getDisplayName(), is("Daniil Penkin"));
        assertThat(author.getLink(), is("https://stash.dev.internal.atlassian.com/users/dpenkin"));

        assertThat(first.getCreatedDate(), is(new Date(1449532120000L)));
        assertThat(first.getUpdatedDate(), is(new Date(1449532130000L)));

        assertThat(first.getReplies().size(), is(0));

        RestCommentAnchor anchor = first.getAnchor();
        assertThat(anchor, is(notNullValue()));
        assertThat(anchor.getFileType(), is("FROM"));
        assertThat(anchor.getLine(), is(12));
        assertThat(anchor.getPath(), is("test/path/README.md"));

        assertThat(first.isEditable(), is(false));
        assertThat(first.isDeletable(), is(true));


        RestComment second = restComments.get(1);
        assertThat(second.getId(), is(131701L));
        List<RestComment> replies = second.getReplies();
        assertThat(replies.size(), is(2));

        RestComment secondReply = replies.get(1);
        assertThat(secondReply.getId(), is(131677L));
        assertThat(secondReply.getReplies().size(), is(1));
    }

    @Test
    public void testParseCommentsFromDiff() {
        JsonObject json = readJsonFromResource("diff.json");
        List<RestComment> restComments = RestUtil.parseDiff(json).getComments();

        assertThat(restComments.size(), is(3));

        RestComment first = restComments.get(0);
        assertThat(first.getText(), is("What's this?"));
        assertThat(first.getAnchor(), is(notNullValue()));
        assertThat(first.getAnchor().getLine(), is(22));
        assertThat(first.getAnchor().getFileType(), is("TO"));
        assertThat(first.getReplies().size(), is(1));

        RestComment second = restComments.get(1);
        assertThat(second.getText(), is("New one"));
        assertThat(second.getAnchor(), is(notNullValue()));
        assertThat(second.getAnchor().getLine(), is(27));
        assertThat(second.getAnchor().getFileType(), is("TO"));
        assertThat(second.getReplies().size(), is(0));

        RestComment third = restComments.get(2);
        assertThat(third.getText(), is("Another new one"));
        assertThat(third.getAnchor(), is(notNullValue()));
        assertThat(third.getAnchor().getLine(), is(27));
        assertThat(third.getAnchor().getFileType(), is("TO"));
        assertThat(third.getReplies().size(), is(0));
    }

    @Test
    public void testParsePullRequest() {
        JsonObject json = readJsonFromResource("pull-request.json");
        RestPullRequest restPullRequest = RestUtil.parsePullRequest(json);

        assertThat(restPullRequest.getId(), is(2L));
        assertThat(restPullRequest.getCreatedDate(), is(new Date(1449532060000L)));
        assertThat(restPullRequest.getTitle(), is("Remove development section from readme"));

        RestUser author = restPullRequest.getAuthor();
        assertThat(author, is(notNullValue()));
        assertThat(author.getDisplayName(), is("Daniil Penkin"));
        assertThat(author.getName(), is("dpenkin"));
        assertThat(author.getLink(), is("https://stash.dev.internal.atlassian.com/users/dpenkin"));
    }

    @Test
    public void testParseUser() {
        JsonObject json = readJsonFromResource("user.json");
        RestUser restUser = RestUtil.parseUser(json);

        assertThat(restUser.getName(), is("dpenkin"));
        assertThat(restUser.getDisplayName(), is("Daniil Penkin"));
        assertThat(restUser.getSlug(), is("dpenkin"));
        assertThat(restUser.getLink(), is("https://stash.dev.internal.atlassian.com/users/dpenkin"));
    }

    @Test
    public void testWriteCommentReply() throws JSONException {
        String expected = readResource("comment-reply.json");
        String actual = RestUtil.writeComment(RestPostComment.reply(24, "This is awesome!"));

        assertEquals(expected, actual, false);
    }

    @Test
    public void testWriteNewComment() throws JSONException {
        String expected = readResource("comment-new.json");
        String actual = RestUtil.writeComment(RestPostComment.create(24, "CONTEXT", "some/path/to/file.txt", "Yes"));

        assertEquals(expected, actual, false);
    }

    protected JsonObject readJsonFromResource(String resourcePath) {
        JsonParser parser = new JsonParser();
        return parser.parse(readResource(resourcePath)).getAsJsonObject();
    }

    private String readResource(String resourcePath) {
        URL url = Resources.getResource(getClass(), resourcePath);
        try {
            return Resources.toString(url, Charsets.UTF_8);
        } catch (IOException e) {
            throw new RuntimeException(String.format("Failed to get resource '%s'", url), e);
        }
    }
}